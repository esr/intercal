/*
 *
 * NAME
 *    ick_lose.c -- report INTERCAL compile- or run-time error
 */
// SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
// SPDX-License-Identifier: GPL-2.0-or-later
/*LINTLIBRARY*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "ick.h"        /* ugh, just for bool */
#include "ick_lose.h"

bool ick_coreonerr; /* AIS */
bool ick_checkforbugs; /* AIS */

/*@-formatconst@*/
void ick_lose(const char *m, int n, const char *s)
{
    (void) fflush(stdout); /* AIS: To stop stdout getting muddled with stderr*/
    (void) fprintf(stderr,
		   "ICL%c%c%cI\t",
		   m[0], m[1], m[2]);
    if (s)
	(void) fprintf(stderr, m + 4, s, n);
    else
	(void) fprintf(stderr, m + 4, n);
    (void) fprintf(stderr, "        CORRECT SOURCE AND RESUBNIT\n");
    if(atoi(m)==778&&ick_coreonerr) /* AIS */
    {
      /* AIS: Dump core. */
      (void) raise(SIGABRT);
    }
    exit(atoi(m));
}

/* AIS: This function reports potential bugs. It's paraphrased from ick_lose. */
void ick_lwarn(const char *m, int n, const char *s)
{
  if(!ick_checkforbugs) return; /* Don't report a potential bug without -l */
  (void) fflush(stdout);
  (void) fprintf(stderr,
		 "ICL%c%c%cW\t",
		 m[0],m[1],m[2]);
  if (s)
    (void) fprintf(stderr, m + 4, s, n);
  else if(m[0]!='2'||m[1]!='7'||m[2]!='8')
    (void) fprintf(stderr, m + 4, n);
  else
      (void) fputs(m + 4, stderr);
  (void) fputs("        RECONSIDER SOURCE AND RESUBNIT\n\n", stderr);
  /* Don't exit. This is not any error except one not causing immediate
     termination of program execution. */
}
/*@=formatconst@*/

/* ick_lose.c ends here */
